
// Please put your own database configurations.
// WARNING :  if you already have database named 'employee' .
//            Please create a new database and update the configuration here as it may overrride your existing database

const databaseConfig = {
    databaseName : 'employee',
    databaseHost : 'localhost',
    databaseUser : 'root',
    databasePassword : ''
}

module.exports =  databaseConfig;