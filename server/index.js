const express = require('express');
const mysql = require('mysql');
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const port = 1300;
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const DBConfig = require('./mysqlconfig.js');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(1312);
// MySql config

const dbCon = mysql.createConnection({
  host: DBConfig.databaseHost,
  user: DBConfig.databaseUser,
  password: DBConfig.databasePassword,
  database: DBConfig.databaseName,
});
dbCon.connect((err) => {
  if (err) throw err
})
dbCon.query('CREATE TABLE users (username VARCHAR(255), phone VARCHAR(255) , email VARCHAR(255) , password VARCHAR(255) , id INT AUTO_INCREMENT PRIMARY KEY)', (err, results) => {
  if (err) {
    if (err.code === 'ER_TABLE_EXISTS_ERROR') {
      //table already exists proceed
      console.log("MySQL:'users' table already exists . Proceeding");
    } else if (err.code !== 'ER_TABLE_EXISTS_ERROR') {
      console.log(err);
      throw err;
    }
  }
})
dbCon.query('CREATE TABLE allEmployees (name VARCHAR(255), phone VARCHAR(255) , age int(3) , salary int(10) , id INT AUTO_INCREMENT PRIMARY KEY)', (err, results) => {
  if (err) {
    if (err.code === 'ER_TABLE_EXISTS_ERROR') {
      //table already exists proceed
      console.log("MySQL:'employee' table already exists . Proceeding");
    } else if (err.code !== 'ER_TABLE_EXISTS_ERROR') {
      console.log(err);
      throw err;
    }
  }

})


// -------- All middlewares -------------
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  cors({
    origin: "http://localhost:3000", // Put company react app address here
    credentials: true,
  })
);
app.use(session({
  secret: 'foo',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false, httpOnly: false } // Only works with https. If secure is set and you access the site over HTTP then cookie will not be set .
}));
app.use(cookieParser("foo"));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


// ------ End middlewares ---------

// ------- passport config (should be after passport init and passport session initialization) --------- //

passport.use(new LocalStrategy(
  {
    usernameField: 'email',
  },
  function (email, password, done) {
    // check from the database if user exists
    let emailStr = email.toString();
    let pass = password.toString();

    let query = `Select * from users where email='${emailStr}'`;

    dbCon.query(query, (err, result, fields) => {
      if (err) throw err;
      if (!err) {
        console.log(result[0]);
        // the result is in the form of RowDataPacket which can accessed like a normal object
        let user = result[0];
        let hash = user.password;
        bcrypt.compare(pass, hash, function (err, bcrypt) {
          if (bcrypt) {
            done(null, user);

          } else {
            done(null, false);
          }
        });
      }
      else {
        console.log("Mysql Error ", err);
        done(null, false);
      }
    })
  }
));

passport.serializeUser(function (user, done) {
  // serializing user data into the cookie
  console.log("Serializing user email", user.email);
  done(null, user.email);
});

passport.deserializeUser(function (obj, done) {
  // deserializeUser gets the serialized cookie value
  // as the user is serialized with email
  // we will deserialize the user with email and get the data from mysql
  let email = obj;
  console.log(email);

  dbCon.query(`Select * from users where email='${email}'`, (err, results, fields) => {
    if (!err) {
      done(null, results[0]);
    } else {
      console.log(err);
      done(null, false);
    }
  })

});
// -------- end passport config ----------
app.use('/login', (req, res, next) => {
  console.log("LOGIN Request body grabbed");
  console.log(req.body);
  next();
})
app.post('/login',
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/loginFailure',
    failureFlash: true
  }),
  (req, res) => {
    console.log('USER', req.user);
    res.send(req.user);
  }
);
app.get('/', (req, res) => {
  console.log("Got User ", req.user);
  res.send(req.user);
})
app.get('/loginFailure', (req, res) => {
  res.send("Login Failed");
})
app.get('/test', (req, res) => {
  var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: ""
  });

  con.connect(function (err) {
    if (err) throw err;
    console.log("MySQL Connected!");
    con.query("CREATE DATABASE employee", function (err, result) {
      if (err.code === 'ER_DB_CREATE_EXISTS') {
        console.error('error connecting: ' + err.code);
        res.send('Database exists Please proceed');
        return;
      } else if (err.code !== 'ER_DB_CREATE_EXISTS') {
        console.error("internal server error ", err);
        res.send("Internal server error");
      }
      else {
        console.log("MySQL connected");
        res.send("Please proceed");
      }
    });
  });
})
app.post('/register', (req, res) => {
  dbCon.query(`Select * from users where email='${req.body.email}'`, (err, results, fields) => {
    if (err) console.log(err);
    if (!results.length) {
      bcrypt.hash(req.body.password, 8, function (err, hash) {
        console.log("HAsh Gen ", hash);
        dbCon.query(`INSERT INTO users (username, phone , email , password) VALUES ('${req.body.name}', '${req.body.phone}','${req.body.email}','${hash}')`, (err, results, fields) => {
          if (err) {
            console.log(err);
            res.sendStatus(503);
          } else {
            res.json({ message: "success" })
          }
        })
      });

    } else if (results.length) {
      res.json({ code: "user_exists" });
    }
  })

})
app.post('/addEmployee', (req, res) => {
  if (req.isAuthenticated()) {
    dbCon.query(`INSERT INTO allEmployees (name , age , salary , phone) VALUES ('${req.body.name}','${req.body.age}','${req.body.salary}','${req.body.phone}')`, (err, results, fields) => {
      if (err) {
        console.log(err);
        res.sendStatus(503);
      } else {
        res.json({ message: "success" })
      }
    })
  } else {
    res.sendStatus(401)
  }
})
app.post('/deleteEmployee', (req, res) => {
  if (req.isAuthenticated()) {
    dbCon.query(`DELETE FROM allemployees WHERE id = ${req.body.id}`, (err) => {
      if (err) {
        console.log(err);
        res.sendStatus(503);
      } else {
        res.json({ message: "success" })
      }
    })
  } else {
    res.sendStatus(401);
  }
})
app.get('/getAll', (req, res) => {
  if (req.isAuthenticated()) {
    console.log("Getting All Employees");
    let sendJSON = new Array;
    dbCon.query(`SELECT * from allEmployees`, (err, results, fields) => {
      if (err) {
        console.log(err);
        res.sendStatus(503);
      } else {
        results.forEach(element => {
          console.log(element.name);
          sendJSON.push(element);
        });
        res.send(sendJSON);
      }

    })
  }
  else {
    res.sendStatus(401);
  }
})
app.get('/logout', (req, res, next) => {
  // It is unreliable as Passport logout methods are still an open issue on github
  // check here https://github.com/jaredhanson/passport/issues/246
  // For a workaround I have decided to delete the cookie named connect.sid on client serialized
  // this eventually logs out the user
  console.log("Logging Out");
  req.logOut();
  req.logout();
  req.session.destroy((err) => {
    if (err) return next(err)

    req.logout()

    res.sendStatus(200)
  });

})
app.get('/isLoggedIn', (req, res) => {
  if (req.isAuthenticated()) {
    res.json({ message: "success" })
  }
  else {
    res.sendStatus(401);
  }
})
app.listen(port, () => {
  console.log("Connected to express on port ", port);
})