# Usage Instruction #

## Prerequisites

Make sure you have MySQL and NodeJs instaled and running on your machine.

## Step 1 - React app setup
In the root folder open a command prompt and run <code>npm install</code> to install the required packages . After installing the packages run <code>npm start</code> to start the react app. You can view the app at <i>http://localhost:3000</i>

## Step 2 - Server setup
Inside root folder you will find 'server folder. Go to the 'server' Folder and open a command prompt there. Run <code>npm install </code> to install the required packages . Then open the file <i>mysqlconfig.js</i> and change the MySql configuration accordingly.<br/>
After that run <code>npm start</code> to start the server in the command prompt

