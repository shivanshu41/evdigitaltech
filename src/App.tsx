import React, { useState } from "react";
import LoginPage from './pages/Login';
import RegisterPage from './pages/Register';
import Dashboard from './pages/Dashboard';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <div className='App'>
            <div className='inner'>
              <LoginPage />
            </div>
          </div>
        </Route>
        <Route exact path="/register">
          <div className='App'>
            <div className='inner'>
              <RegisterPage />
            </div>
          </div>
        </Route>
        <Route exact path="/dashboard">
          <div className="App">
            <Dashboard />
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;