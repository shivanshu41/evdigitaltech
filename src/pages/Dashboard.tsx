import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Table, Button, Form } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import './Dashboard.css';
const Dashboard: React.FC = () => {
    const history = useHistory();
    const [tableActive, setTableActive] = useState<Boolean>(true);
    const [loggedIn, setLoggedIn] = useState<Boolean>(false);
    const [name, setName] = useState('');
    const [age, setAge] = useState<Number | null>(null);
    const [salary, setSalary] = useState<Number | null>(null);
    const [phone, setPhone] = useState<string | null>(null);
    const [tableData, setTableData] = useState<any[] | null>(null);
    useEffect(() => {
        Axios.get('http://localhost:1300/getAll', {
            withCredentials: true
        })
            .then((res) => {
                console.log(res);
                setLoggedIn(true);
                setTableData((prevState: any) => {
                    return res.data;
                })
            })
            .catch((e) => {
                console.log(e);
                setLoggedIn(false);
                history.push('/');
            })
    }, [loggedIn, history])
    const handleAddEmployee = (e: React.SyntheticEvent) => {
        e.preventDefault();
        Axios({
            method: "POST",
            data: {
                name: name,
                age: age,
                phone: phone,
                salary: salary
            },
            withCredentials: true,
            url: "http://localhost:1300/addEmployee"
        })
            .then((res) => {
                console.log("Add ", res.data.message);
                console.log("Getting All");
                return Axios.get('http://localhost:1300/getAll', { withCredentials: true })
                    .then((emp) => {
                        console.log(emp.data);
                        setTableData((prevState: any) => {
                            return emp.data;
                        })
                    })
                    .catch((e) => {
                        console.log(e);
                    })
            })
            .catch((e) => {
                console.log(e);
            })
    }
    const handleDeleteEmp = (index: string) => {
        let indexInteger = parseInt(index);
        let employee = tableData![indexInteger];
        let employeeId = employee.id;
        Axios({
            method: "POST",
            url: "http://localhost:1300/deleteEmployee",
            data: {
                id: parseInt(employeeId)
            },
            withCredentials: true
        }).then((res) => {
            console.log(res.data.message);
            setTableData((prevState) => {
                console.log(prevState);
                let newArray = prevState!.filter((element, index) => {
                    return index !== indexInteger
                });
                return newArray;
            });
        })
            .catch((e) => {
                console.log(e);
            })
    }
    const handleLogOut = () => {
        Axios.get('http://localhost:1300/logout')
            .then((res) => {
                console.log(res);
                setLoggedIn(false);
                document.cookie = "connect.sid=null"
            })
            .catch((e) => {
                console.log(e);
            })
    }
    const preRender = () => {
        if (loggedIn) {
            return (
                <div className={'dash'}>
                    <Table striped responsive bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Salary</th>
                                <th>Phone</th>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            {tableData?.map((employee: any, index: any) => {
                                return <tr>
                                    <td>{employee.id}</td>
                                    <td>{employee.name}</td>
                                    <td>{employee.age}</td>
                                    <td>{employee.salary}</td>
                                    <td>{employee.phone}</td>
                                    <td><button data-index={index} onClick={(e) => handleDeleteEmp(e.currentTarget.attributes['data-index' as any].value)} className={'empDelete'}>Delete</button></td>
                                </tr>
                            })}
                        </tbody>
                    </Table>

                    <Form onSubmit={(e) => handleAddEmployee(e)} style={{ display: "flex", flexDirection: "column" }}>
                        <Form.Group controlId="formBasicEmail" className={'addEmployeeForm'}>
                            <Form.Control type="text"
                                required
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Name" />
                            <Form.Control type="number"
                                required
                                maxLength={3}
                                onChange={(e) => setAge(parseInt(e.target.value))}
                                placeholder="Age" />
                            <Form.Control type="number"
                                required
                                onChange={(e) => setSalary(parseInt(e.target.value))}
                                placeholder="Salary" />
                            <Form.Control type="tel" pattern="^[6789][0-9]{9}"
                                maxLength={10}
                                onChange={(e) => setPhone(e.target.value)}
                                required
                                placeholder="Phone" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Add Employee
                            </Button>
                    </Form>
                    <Button style={{ background: "#F44336" ,border:"none",marginTop:"1.5em"}} variant="primary" onClick={handleLogOut}>
                        Logout
                            </Button>
                </div>
            )
        }
    }
    return <>
        {preRender()}
    </>
}

export default Dashboard;