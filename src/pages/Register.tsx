import React, { useState } from 'react';
import { Button, Form, Alert } from 'react-bootstrap';
import Axios from 'axios';
import { Link } from 'react-router-dom';
const RegisterPage: React.FC = () => {
    const [name, setName] = useState<string>('');
    const [pass, setPass] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [phone, setPhone] = useState<string>('');
    const [registered, setRegistered] = useState<boolean>(false);
    const [userExists, setUserExists] = useState<boolean>(false);
    const register = (e: React.SyntheticEvent) => {
        e.preventDefault();
        Axios({
            method: "POST",
            data: {
                name: name,
                email: email,
                phone: phone,
                password: pass
            },
            withCredentials: true,
            url: "http://localhost:1300/register",
        }).then((res) => {
            if (res.data.code === 'user_exists') {
                setRegistered(false);
                setUserExists(true);
            } else {
                setRegistered(true)
                setUserExists(false);

            }
        })
            .catch((e) => {
                setRegistered(false);
            });
    };
    const preRender = () => {
        if (registered) {
            return <div className={'successReg'} >
                You have successfully registered. Please <Link to={'/'} style={{ color: "#ffffff", textDecoration: "underline" }}>Login Here</Link>
            </div>
        } else {
            if (userExists) {
                return <div className={'successReg'} style={{backgroundColor:"#FF4081"}} >
                    User already exists. Please <Link to={'/'} style={{ color: "#ffffff", textDecoration: "underline" }}>Login Here</Link>
                </div>
            } else {
                return <>
                    <Form onSubmit={(e) => {
                        e.preventDefault();
                        register(e);
                        return false
                    }}>
                        <Form.Group controlId="registerName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control required type="text"
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Enter your name" />
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control required type="email"
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="Enter your email" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Phone</Form.Label>
                            <Form.Control required type="tel"
                                pattern="^[6789][0-9]{9}"
                                maxLength={10}
                                onChange={(e) => setPhone(e.target.value)}
                                placeholder="Enter your phone number" />
                            <Form.Text className="text-muted">
                                It should be a valid Indian phone number without country code
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control required type="password"
                                onChange={(e) => setPass(e.target.value)}
                                placeholder="Create a Password" />
                        </Form.Group>
                        <Button variant="secondary" type="submit">
                            Register Me
                        </Button>
                    </Form>
                    <div className={'goTo'}>Already registered ? <Link to={'/'}>Login Here</Link></div>
                </>
            }
        }
    }
    return <>
        <h1>Register</h1>
        {preRender()}
    </>
}
export default RegisterPage;