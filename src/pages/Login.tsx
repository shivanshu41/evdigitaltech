import React , {useState,useEffect} from 'react';
import { Button, Form } from 'react-bootstrap';
import Axios from 'axios';
import {Link,useHistory} from 'react-router-dom';
const LoginPage: React.FC = () => {
    const history = useHistory();
    const [loginUsername, setLoginUsername] = useState("");
    const [loginPassword, setLoginPassword] = useState("");
    useEffect(()=>{
        Axios.get('http://localhost:1300/isLoggedIn',{withCredentials:true})
        .then((res)=>{
            history.push('/dashboard')
        })
    },[]);
    const login = (e:React.SyntheticEvent) => {
        e.preventDefault();
        console.log('logging In');
        Axios({
            method: "POST",
            data: {
                email: loginUsername,
                password: loginPassword,
            },
            withCredentials: true,
            url: "http://localhost:1300/login",
        }).then((res)=>{
            history.push('/dashboard');
        });
    };
    return <>
        <h1>Log In</h1>
        <Form onSubmit={(e)=>{
                e.preventDefault();
                login(e);
                return false
                }}>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control required type="email" placeholder="Enter email" onChange={(e) => setLoginUsername(e.target.value)} />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
            </Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control required type="password" placeholder="Password"  onChange={(e) => setLoginPassword(e.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit">
                Log Me In
            </Button>
        </Form>
        <div className={'goTo'}>Didn't Registered yet ? <Link to={'/register'}>Register Here</Link></div>
    </>
}

export default LoginPage;